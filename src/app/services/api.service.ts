import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}
  Key = "clll6tQOFGhPlddZmmGl";
  page = 1;
   getImage() {
    return this.httpClient.get(
      `https://cacoo.com/api/v1/diagrams.json?apiKey=${this.Key}`     

    );
  }
}
